import tornado.ioloop
import tornado.web
from base.db import Database
import proto
from base.proto_utils import json_format
from modules import user_module
from tornado import ioloop, gen
import pymongo


class MainHandler(tornado.web.RequestHandler):
    def initialize(self, modules):
        self.modules = modules

    async def get(self):
        user = proto.User()
        user.username = "Ali2"
        user.password = "2578o"
        user.role = proto.User.BLOG_USER
        try:
            await self.modules['user_db'].create_user(user)
        except pymongo.errors.DuplicateKeyError as e:
            print(e)


@gen.coroutine
def make_app():
    db = Database()
    db = yield db.get_db()
    modules = {}
    user_db = user_module.UserModule(db)
    user_db.BuildDB()
    modules['user_db'] = user_db
    raise gen.Return(tornado.web.Application([
        (r"/", MainHandler, dict(modules=modules)),
    ],

    ))


if __name__ == "__main__":
    app = ioloop.IOLoop.current().run_sync(make_app)
    app.listen(8889)
    tornado.ioloop.IOLoop.current().start()
