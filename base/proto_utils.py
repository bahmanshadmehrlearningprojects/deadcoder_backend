from google.protobuf import json_format as jf


def json_format(obj):
    data = jf.MessageToDict(obj, preserving_proto_field_name=True)
    return data
