class BaseModule():

    '''
    This class is created for handling our CRUD operations
    '''

    def __init__(self, db, table, indexes=[]):
        self._collection = db[table]

    def get_connection(self):
        return self._collection

    async def create(self, object):
        await self._collection.insert_one(
            object
        )

    async def read(self, to_find):
        r = await self._collection.find_one(
            to_find
        )
        return r

    async def update(self, to_find, to_change, upsert=False):
        await self._collection.update_one(
            to_find,
            to_change,
            upsert=upsert
        )

    async def delete(self, to_find):
        await self._collection.delete_one(
            to_find
        )

    async def create_many(self, objects):
        await self._collection.insert_many(
            objects
        )

    async def read_many(self, to_find):
        await self._collection.find(
            to_find
        )

    async def update_many(self, to_find, to_change, upsert):
        await self._collection.update(
            to_find,
            {'$set': to_change},
            upsert=upsert
        )

    async def delete_many(self, to_find):
        self._collection.delete(
            to_find
        )
