from base import security
import json

class Authentication():
    def generate_access_tocken(self, user):
        data = {
            'user': user,
        }
        json_data = json.dumps(data)
        access_tocken = security.encode_jwt(json_data)
        return access_tocken

    def decode_access_tocken(self, access_tocken):
        json_data = security.decode_jwt(access_tocken)
        data = json.dumps(json_data)
        return data['user']