from Crypto.Hash import HMAC
import jwt

_SECRET = "DeadCoder@#!"


def encode_jwt(token):
    return jwt.encode(token, _SECRET, algorithm='HS256')


def decode_jwt(encoded_token):
    return jwt.decode(encoded_token, _SECRET)


def encode_(msg):
    return HMAC.new(_SECRET, msg).hexdigest()
