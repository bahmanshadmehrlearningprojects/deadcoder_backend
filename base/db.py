import motor.motor_tornado


class Database():

    '''
    This class has been created for having our main database system and our main client
    '''

    def __init__(self):
        self._client = motor.motor_tornado.MotorClient('localhost', 27017)
        self._db = self._client['test_database']

    async def get_db(self):
        return self._db
