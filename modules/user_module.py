from base.module import BaseModule
from base.proto_utils import json_format
from bson import ObjectId
import pymongo


class UserModule(BaseModule):
    def __init__(self, db):
        super(UserModule, self).__init__(db, "users")

    def BuildDB(self):
        connection = self.get_connection()
        connection.create_index(
            [("username", pymongo.ASCENDING)],
            unique=True
        )

    async def create_user(self, user):
        db_obj = json_format(user)
        r = await self.create(db_obj)
        print(r)

    async def read_user(self, user):
        to_find = {}
        if user.id:
            to_find["_id"] = ObjectId(user.id)
        if user.username:
            to_find["username"] = user.username
        return await self.read(to_find)

    async def update_user(self):
        pass

    async def delete_user(self):
        pass
